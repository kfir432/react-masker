import React from 'react';

const Button = (props) => {
    const {text} = props;
    return (
        <button type="button" className="form-control btn btn-primary" onClick={props.onClick}>{text}</button>
    )
};

export {Button};