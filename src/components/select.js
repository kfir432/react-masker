import React from 'react';

const Select = (props) => {
    const {id, options,label} = props;
    return (
        <div className="form-group">
            <label>{label}</label>
            <select className="form-control" onChange={props.onChange} id={id}>
                {options.map(value => {
                    return <option value={value} key={value}>{value}</option>
                })}
            </select>
        </div>
    )
};

export {Select};