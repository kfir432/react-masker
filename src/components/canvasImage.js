import React, {Component} from 'react';
import { Stage,Layer } from "react-konva";
import Konva from "konva/konva";
import {observer} from 'mobx-react';

@observer
export default class CanvasImage extends React.Component {


    componentDidUpdate() {
        let stage = new Konva.Stage({
            container: 'container',
            width: 700,
            height: 500
        });

        let layer = new Konva.Layer();
        let layer2 = new Konva.Layer();

        stage.add(layer2, layer);

        let mask = new Image();
        mask.onload = () => {
            let img = new Image();
            img.onload = () => {
                let image = new Konva.Shape({
                    sceneFunc: (ctx) => {
                        ctx.save();
                        ctx.drawImage(mask, -image.x(), -image.y(), 700, 500);
                        ctx.globalCompositeOperation = 'source-in';
                        ctx.drawImage(img, 0, 0, 900, 700);
                        ctx.globalCompositeOperation = 'source-over';
                        ctx.restore();
                    },


                    // (!) important
                    // for this case you need to define custom hit function
                    hitFunc: (ctx) => {
                        ctx.rect(0, 0, 700, 500);
                        ctx.fillStrokeShape(image);
                    },
                    draggable: true,
                });
                layer.add(image);
                layer.draw();
            };
            img.src = this.props.image;

        };
        mask.src = './img/'+this.props.mask;
    }

    render() {
        return (
           <div id="container"></div>
        )
    }
}
