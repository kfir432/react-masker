import React, {Component} from 'react';
import {Button, Select} from './index';
import Image from '../stores/Image';
import {observer} from 'mobx-react';
import {saveAs} from 'file-saver';
import html2canvas from 'html2canvas';
import pcsJson from '../api/pcs';
import domtoimage from 'dom-to-image';
@observer
class Tools extends Component {
    constructor(props) {
        super(props);
        this.state = {
            file: '',
            imagePreviewUrl: '',
            pcsSrc: ''
        };
    }

    _handleImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({file: file, imagePreviewUrl: reader.result});
            Image.src = this.state.imagePreviewUrl;
        };
        reader.readAsDataURL(file)
    }

    _cutImageToPieces(e) {
        let valueSelected = e.target.value;
        this.setState({pcsSrc: valueSelected});
        Image.pcsSvgSrc = valueSelected;

        // let canvas = document.getElementById("canvas");
        // let ctx = canvas.getContext("2d");
        // ctx.save();
        // //console.log(pcsJson[valueSelected]);
        // pcsJson[valueSelected].map(pcs => {
        //     let x = pcs.rect[0];
        //     let y = pcs.rect[1];
        //     let width = pcs.rect[2];
        //     let height = pcs.rect[3];
        //     ctx.rect(x,y,width,height);
        //     ctx.fillRect(x,y,width,height);
        //     ctx.shadowBlur = 7;
        //     ctx.shadowColor = "#000";
        //     ctx.fillStyle = "#fff";
        //     ctx.stroke();
        // });
        //
        // ctx.clip();
        //
        // let img = document.getElementById("data-image");
        // ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
        // ctx.restore();
    }


    _handleWidthChange(e) {
        let width = e.target.value;
        Image.width = width;
    }

    _handleBgChange(e) {
        let valueSelected = e.target.value;
        Image.backgroundImage = valueSelected;
    }
    _saveImage() {
        let fileName = `${this.state.file.name}-${Image.pcsSvgSrc}`;
        let node = document.getElementById('dom-image');
        html2canvas(node, {
            allowTaint: true,
            onrendered: function (canvas) {
                document.body.appendChild(canvas);
            }
        });


        // html2canvas(node).then((canvas) => {
        //     canvas.toBlob((blob) => {
        //         // Generate file download
        //
        //         let base64image = canvas.toDataURL("image/png");
        //
        //         // Split the base64 string in data and contentType
        //         let block = base64image.split(";");
        //         // Get the content type
        //         let mimeType = block[0].split(":")[1];// In this case "image/png"
        //         // get the real base64 content of the file
        //         let realData = block[1].split(",")[1];// For example:  iVBORw0KGgouqw23....
        //
        //         // Convert b64 to blob and store it into a letiable (with real base64 as value)
        //         let canvasBlob = this._b64toBlob(realData, mimeType);
        //
        //         saveAs(canvasBlob, "yourwebsite_screenshot.png");
        //     });
        // });
        domtoimage.toBlob(node).then(function (blob) {
            saveAs(blob, fileName);
        });
    }

    render() {
        return (
            <div>
                <div className="form-group">
                    <label>Upload Image</label>
                    <input
                        className="form-control"
                        type='file'
                        id="imgInp"
                        onChange={(e) => this._handleImageChange(e)}/>
                </div>
                <div className="form-group">
                    <label>Width</label>
                    <input
                        className="form-control"
                        type='number'
                        value={Image.width}
                        onChange={(e) => this._handleWidthChange(e)}/>
                </div>
                <Select
                    options={['3pcsEq.png', '4pcs', '5pcs.png']}
                    id="pcs"
                    label="Choose Pcs"
                    onChange={(e) => this._cutImageToPieces(e)}/>
                <Select
                    options={['bg1', 'bg2', 'bg3']}
                    id="bg"
                    label="Choose BG"
                    onChange={(e) => this._handleBgChange(e)}
                />
                <div className="form-group">
                    <Button onClick={() => this._saveImage()} text="Save Image"/>
                </div>

            </div>
        )
    }
}

export default Tools;
