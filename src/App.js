import React, {Component} from 'react';
import './App.css';
import Tools from './components/tools';
import Image from './stores/Image';
import {observer} from 'mobx-react';
import CanvasImage from './components/canvasImage';
import {Stage, Layer, Rect, Text} from 'react-konva';

@observer
class App extends Component {
    constructor(props) {
        super(props);
        this._moveImage = this._moveImage.bind(this);
        this.state = {}
    }

    _moveImage(e) {
        let canvas = document.getElementById("canvas");
        let ctx = canvas.getContext("2d");
        let img = document.getElementById("data-image");

        let canvasHeight = canvas.height;
        console.log(canvasHeight);
        ctx.drawImage(img, 30, 30);

    }

    render() {

        let imgStyle = {
            // WebkitMask:'url(./img/'+Image.pcsSvgSrc+'.svg) top / cover',
            // // -o-mask: url(./img/3pcsEq.svg) top / cover;
            // // -ms-mask: url(./img/3pcsEq.svg) top / cover;
            // width: Image.width+'px',
            // margin: '0 auto',
            display: 'none'
        };
        let imgHolderStyle = {
            backgroundImage: `url(./img/${Image.backgroundImage}.jpg)`,
            height: '100vh',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat'
        };
        return (
            <div className="App">
                <div className="col-md-3" style={{position: 'fixed', top: 0, left: 0}}>
                    <form id="form1">
                        <div id="tools">
                            <Tools/>
                        </div>
                    </form>
                </div>


                <div style={imgHolderStyle} id="dom-image">
                    <CanvasImage image={Image.src} mask={Image.pcsSvgSrc}/>

                    {!Image.src
                        ? <span>Please Select Image</span>
                        : <img id="data-image" src={Image.src} style={imgStyle} alt=""/>}
                </div>

            </div>
        );
    }
}

export default App;
