import {observable, action, computed} from 'mobx';

class Image {

    @observable src;
    @observable title;
    @observable width = 650;
    @observable height;
    @observable pcsSvgSrc = '3pcsEq.png';
    @observable quality = 1;
    @observable backgroundImage = 'bg1';

    /**
     * Resets image instance back to initial
     * @class Image
     */
    @action reset = () => {
        this.src = '';
        this.title = '';
        this.width = '';
        this.height = '';
        this.backgroundImage = '';

    };

    constructor(image) {
        Object.assign(this, image);
    }

    /**
     * Update Image instance by another "image" object
     * @class Image
     * @param {image} image is a JSON object that fits to Image object structure
     */
    @action assignImage(image) {
        Object.assign(this, image);
        //TODO decide if we want to update image location each time we update image details
    }


}

const imageStore = new Image();

export default imageStore;
export {Image};